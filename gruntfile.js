module.exports = function(grunt) {

	'use strict';

	require('jit-grunt')(grunt, {
		scsslint: 'grunt-scss-lint'
	});

	if (grunt.option('verbose')) {
		require('time-grunt')(grunt);
	}

	grunt.config.set('paths', {
		rjs: {
			bower: '../../bower_components'
		}
	});

	// tasks estão separadas no diretório grunt
	grunt.loadTasks('grunt');

	grunt.config.set('concurrent', { //run connect and watch tasks in parallel
		target: {
			tasks: ["connect", "watch"],
			options: {
				logConcurrentOutput: true
			}
		}
	});

	grunt.config.set('exec', {
		'touch': 'touch noop.config',
		'bower': 'bower install -F',
		'bundle': 'bundle install'
	});

	grunt.registerTask('setup',
		'Faz o startup do projeto;',
		[
			'githooks', 'exec:bower', 'exec:bundle',
			'default'
		]);

	grunt.registerTask('lint',
		'Valida o projeto com jshint e scsslint;',
		['scsslint', 'jshint']);

	grunt.registerTask('dev',
		'Cria servidor e executa a task watch em paralelo',
		['concurrent']);

	grunt.registerTask('default',
		'Task padrão gera o css e js no modo "produção".',
		['css', 'js', 'jekyll']);
};
