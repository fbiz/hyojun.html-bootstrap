module.exports = function(grunt) {

	'use strict';

	grunt.config.set('jekyll.dist', {
		options: {
			bundleExec: true,
			config: "_config.yml"
		}
	});

	grunt.config.set('connect.server', {
		options: {
			port: 3000,
			base: "./_site",
			keepalive: true
		}
	});

	grunt.config.set('watch.jekyll', {
		options: {
			"livereload": true,
		},
		files: [
			"assets/js/dist/**/*.js",
			"assets/**/*.css",
			"**/*.html",
			"!_site/**/*"
		],
		tasks: ["jekyll"]
	});
};
