module.exports = function(grunt) {
	grunt.config.set('watch', {
		jekyll: {
			files: [
				'**/*.html',
				'!_site/**/*.html'
			],
			tasks: ['jekyll', 'exec:touch']
		},
		requirejs_partial: {
			files: [
				'assets/**/*.js',
				'!assets/js/dist/**/*.js'
			],
			options: {
				livereload: true,
				spawn: false
			},
			tasks: ['requirejs_partial', 'jekyll', 'exec:touch']
		},
		sass: {
			files: ['assets/sass/**/*.scss'],
			tasks: ['css:dev', 'jekyll', 'exec:touch']
		},
		noop: {
			options: {
				livereload: true
			},
			files: 'noop.config'
		}
	});
};
