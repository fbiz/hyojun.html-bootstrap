module.exports = function(grunt) {

	'use strict';

	var DependencyChecker = require('./modules/dependency-checker')(grunt);
	var requireConfig = grunt.file.readJSON('require.config.json') || {};

	grunt.config.set('requirejs', (function() {
		var config = (function() {
				var config = {
					options: {
						optimize: 'uglify2'
					}
				};

				if (!requireConfig.optimize) {
					requireConfig.optimize = config.options.optimize;
				}

				config.options = requireConfig;

				config.options.onBuildRead = function(mod, path, content) {
					if (config.options.syncModules.indexOf(mod) > -1) {
						return 'queue(function(){ ' + content + '});';
					}
					return content;
				};
				return config;
			}()),
			excludeFromPages = new DependencyChecker({
				'root': config.options.baseUrl,
				'appRoot': config.options.appFolder
			}).getAllDependenciesFrom(requireConfig.structureApp);

		grunt.file.recurse(config.options.baseUrl + '/' + config.options.appFolder, function(abs, root, sub, file){
			if (file.match(/\.js$/i)) {
				var name = file.replace(/\.js/i, ''),
					module = config.options.appFolder + name;

				config[name] = {
					'options': {
						'name': module,
						'exclude': name === 'structure' ? [] : excludeFromPages,
						'out': config.options.baseUrl + '/' + config.options.outputFolder + file
					}
				};
			}
		});

		return config;
	}()));

	// 'PRIVATE' TASKS (escondendo do help)
	if (!grunt.option('help')) {
		grunt.task.registerTask('requirejs_partial',
			'Roda a requirejs baseado num arquivo de sub-module.',
			function(file) {

				var dependencyChecker = new DependencyChecker({
						'root': requireConfig.baseUrl,
						'appRoot': requireConfig.appFolder
					}),
					appList = [],
					module,
					excludedFromPages = new DependencyChecker({
						'root': requireConfig.baseUrl,
						'appRoot': requireConfig.appFolder
					}).getAllDependenciesFrom(requireConfig.structureApp);

				file = file || grunt.config('requirejs_partial.file');
				module = dependencyChecker.convertFileToModule(file);
				module = module.replace(requireConfig.appFolder, '');
				grunt.config('requirejs.options.optimize', 'none');

				if (excludedFromPages.indexOf(module) >= 0) {
					appList = ['requirejs:' + requireConfig.structureApp.replace(requireConfig.appFolder, '')];
				} else {
					appList = dependencyChecker
						.getAllAppRelatedTo(module)
						.map(function (val) {
							return 'requirejs:' + val;
						});
				}

				if (appList.length === 0 && dependencyChecker.isFileApp(file)) {
					appList = ['requirejs:' + module];
				}

				grunt.task.run(appList);
				return appList;
			});
	}

	grunt.event.on('watch', function(action, filepath, target) {
		if (target === 'requirejs_partial') {
			grunt.config('requirejs.options.optimize', 'none');
			grunt.config('requirejs_partial.file', filepath);
		}
	});

	grunt.config.set('jshint', {
		'options': {
			'jshintrc': '.jshintrc',
			'reporterOutput':''
		},
		'main': [
			'gruntfile.js',
			'assets/js/**/*.js',
			'!assets/js/dist/**/*.js'
		]
	});

	grunt.registerTask('js:dev',
		'Gera os javascripts no modo de "desenvolvimento": não valida ou comprime;',
		function() {
			grunt.config('requirejs.options.optimize', 'none');
			grunt.task.run('requirejs');
		});

	grunt.registerTask('js',
		'Gera os javascript no modo de "produção", validando e executando o uglify;',
		['jshint', 'requirejs']);
};
