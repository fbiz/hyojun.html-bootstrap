module.exports = function(grunt) {

	'use strict';

	grunt.config.set('sass', {
		'main': {
			'options': {
				'includePaths': [
					'bower_components'
				],
				'outputStyle': 'compressed',
				'sourcemap': true
			},
			'files': [{
				'expand': true,
				'cwd': 'assets/sass/output',
				'src': ['**/*.scss'],
				'dest': 'assets/css',
				'ext': '.css'
			}]
		}
	});

	grunt.config.set('scsslint', {
		main: [
			'assets/sass/**/*.scss'
		],
		options: {
			bundleExec: true,
			colorizeOutput: true,
			config: '.scss-lint.yml'
		}
	});

	grunt.registerTask('css:dev',
		'Gera os css no modo "desenvolvimento": não valida ou comprime;',
		function() {
			grunt.config('sass.main.options.outputStyle', 'nested');
			grunt.config('sass.main.options.sourcemap', false);
			grunt.task.run('sass');
		});

	grunt.registerTask('css',
		'Gera os css no modo de "produção", validando e comprimindo;',
		['scsslint', 'sass']);
};
