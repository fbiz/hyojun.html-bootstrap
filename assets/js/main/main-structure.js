define(function (require) {
	// Almond por sempre ser saída concatenada
	require('almond');
	require('zepto/zepto');
	require('zepto/event');
	// Arquivo inserido para garantir que 2 arquivos
	// sejam carregados em paralelo e executem na sequencia
	require('modules/hyojun/mod-async-run');
});

require([], function() {
	// Inicializações vem aqui
});
