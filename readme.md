# Hyojun.html-bootstrap

`1.0.1`

Bootstrap para produção de sites simples em HTML utilizando o Jekyll como gerador de páginas.

## Setup

Instale os seguintes programas:

1. [Ruby](http://www.ruby-lang.org/pt/downloads/) — `2.2` ou `brew install ruby`* ou via rvm `curl -sSL https://get.rvm.io | bash -s stable --ruby`;
4. [NodeJS](http://nodejs.org) — `0.12` ou `brew install nodejs`*;
5. Bundler `1.10` — `gem install bundler -v '~>1.10'`;
6. Bower `1.6` — `npm install -g bower@^1.6`;
7. GruntCLI `0.1` — `npm install -g grunt-cli@^0.1`;
8. NPM `3.3` — `npm install -g npm@^3.3`;

_*No caso do Mac com [homebrew](http://brew.sh/) instalado._

Clone o projeto e rode as tarefas:

    $ git clone git@bitbucket.org:fbiz/hyojun.html-bootstrap.git
    $ cd hyojun.html-bootstrap/ && npm install && grunt setup

Rode o comando:

    $ grunt dev

Para iniciar o `watch` (que monitora as alterações nos `scss`, `js` e `html`) e montar um servidor em http://localhost:3000/.

## Organização

O site será gerado no diretório `./_site`. Tudo que for colocado na raíz do projeto será inserido neste diretório, com exceção do que aparece no exclude list do jekyll em `_config.yml`.

### _layouts

Diretório contém as "masterpages". Por padrão todos os HTMLs utilizarão o `default.html`.

### _includes

Diretório contém arquivos que podem ser incluídos no projeto através do comando:

    {% include partial.html %}

Também é possível passar parâmetros:

    {% include partial.html param1="teste" %}

Que ficam acessíveis através de `{{ include.param1 }}`.

### Criando páginas

Apenas crie os arquivos html com a estrutura desejada que o jekyll processará e jogará em `_site`.
Para apontar para a raíz do site, utilize a variável `{{ site.url }}`. Ex.:

    <a href="{{ site.url }}/pagina.html">Voltar para a home</a>

## Avançado

Para mais informações de como configurar e utilizar o Jekyll e o template system Liquid, acesse:

* http://liquidmarkup.org/
* http://jekyllrb.com/docs/home/
